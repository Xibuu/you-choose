module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    "comma-dangle": ["error", "never"],
    "linebreak-style": "off",
    'max-len': ["error", { "code": 5000 }],
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
    'import/extensions': [ 'error', 'ignorePackages', { js: 'never', jsx: 'never', }, ],
  },
};
